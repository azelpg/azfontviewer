# azfontviewer

http://azsky2.html.xdomain.jp/

フォントビューア。<br>
フォントファイルから直接テキストをプレビューします。<br>
縦書き対応。

## 動作環境

- Linux、macOS(要XQuartz) ほか
- X11

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~

