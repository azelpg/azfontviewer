/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_ARGPARSE_H
#define MLK_ARGPARSE_H

typedef struct _mArgParse mArgParse;
typedef struct _mArgParseOpt mArgParseOpt;


struct _mArgParse
{
	int argc;
	char **argv;
	mArgParseOpt *opts,
		*curopt;
	uint32_t flags;
};

struct _mArgParseOpt
{
	const char *longopt;
	int16_t opt;
	uint16_t flags;
	void (*func)(mArgParse *p,char *arg);
};


enum MARGPARSE_FLAGS
{
	MARGPARSE_FLAGS_UNKNOWN_IS_ARG = 1<<0
};

enum MARGPARSEOPT_FLAGS
{
	MARGPARSEOPT_F_HAVE_ARG = 1<<0,
	MARGPARSEOPT_F_PROCESSED = 1<<15
};


#ifdef __cplusplus
extern "C" {
#endif

int mArgParseRun(mArgParse *p);

#ifdef __cplusplus
}
#endif

#endif
