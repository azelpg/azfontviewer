/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_SPLITTER_H
#define MLK_SPLITTER_H

#define MLK_SPLITTER(p)     ((mSplitter *)(p))
#define MLK_SPLITTER_DEF(p) mWidget wg; mSplitterData spl;

typedef struct
{
	mWidget *wgprev,*wgnext;
	int prev_min,prev_max,prev_cur,
		next_min,next_max,next_cur;
	intptr_t param;
}mSplitterTarget;

typedef int (*mFuncSplitterGetTarget)(mSplitter *,mSplitterTarget *);


typedef struct
{
	uint32_t fstyle;
	int press_pos,
		drag_diff,
		fpointer;
	mSplitterTarget info;
	mFuncSplitterGetTarget get_target;
}mSplitterData;

struct _mSplitter
{
	mWidget wg;
	mSplitterData spl;
};


enum MSPLITTER_STYLE
{
	MSPLITTER_S_HORZ = 0,
	MSPLITTER_S_VERT = 1<<0
};

enum MSPLITTER_NOTIFY
{
	MSPLITTER_N_MOVED
};


#ifdef __cplusplus
extern "C" {
#endif

mSplitter *mSplitterNew(mWidget *parent,int size,uint32_t fstyle);
void mSplitterSetFunc_getTarget(mSplitter *p,mFuncSplitterGetTarget func,intptr_t param);

void mSplitterDestroy(mWidget *p);
void mSplitterHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mSplitterHandle_event(mWidget *wg,mEvent *ev);

#ifdef __cplusplus
}
#endif

#endif
