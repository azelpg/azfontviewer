/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_SCROLLBAR_H
#define MLK_SCROLLBAR_H

#define MLK_SCROLLBAR(p)  ((mScrollBar *)(p))
#define MLK_SCROLLBAR_DEF mWidget wg; mScrollBarData sb;
#define MLK_SCROLLBAR_WIDTH  15

typedef void (*mFuncScrollBarHandle_action)(mWidget *p,int pos,uint32_t flags);

typedef struct
{
	mFuncScrollBarHandle_action action;
	uint32_t fstyle;
	int min, max, page, pos,
		range, drag_diff, fgrab;
}mScrollBarData;

struct _mScrollBar
{
	mWidget wg;
	mScrollBarData sb;
};

enum MSCROLLBAR_STYLE
{
	MSCROLLBAR_S_HORZ = 0,
	MSCROLLBAR_S_VERT = 1<<0
};

enum MSCROLLBAR_NOTIFY
{
	MSCROLLBAR_N_ACTION
};

enum MSCROLLBAR_ACTION_FLAGS
{
	MSCROLLBAR_ACTION_F_CHANGE_POS = 1<<0,
	MSCROLLBAR_ACTION_F_PRESS = 1<<1,
	MSCROLLBAR_ACTION_F_DRAG  = 1<<2,
	MSCROLLBAR_ACTION_F_RELEASE = 1<<3
};


#ifdef __cplusplus
extern "C" {
#endif

mScrollBar *mScrollBarNew(mWidget *parent,int size,uint32_t fstyle);
mScrollBar *mScrollBarCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);
void mScrollBarDestroy(mWidget *p);

void mScrollBarHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mScrollBarHandle_event(mWidget *wg,mEvent *ev);

mlkbool mScrollBarIsPos_top(mScrollBar *p);
mlkbool mScrollBarIsPos_bottom(mScrollBar *p);
int mScrollBarGetPos(mScrollBar *p);
int mScrollBarGetPage(mScrollBar *p);
int mScrollBarGetMaxPos(mScrollBar *p);

void mScrollBarSetHandle_action(mScrollBar *p,mFuncScrollBarHandle_action action);
void mScrollBarSetStatus(mScrollBar *p,int min,int max,int page);
int mScrollBarSetPage(mScrollBar *p,int page);
mlkbool mScrollBarSetPos(mScrollBar *p,int pos);
mlkbool mScrollBarSetPos_bottom(mScrollBar *p);
mlkbool mScrollBarAddPos(mScrollBar *p,int val);

#ifdef __cplusplus
}
#endif

#endif
