/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * <X11> カーソル
 *****************************************/

#define MLKX11_INC_XCURSOR
#include "mlk_x11.h"

#include "mlk_cursor.h"


/** カーソル解放 */

void __mX11CursorFree(mCursor cur)
{
	if(cur)
		XFreeCursor(MLKX11_DISPLAY, (Cursor)cur);
}

/** デフォルトテーマから指定名のカーソル読み込み
 *
 * return: 読み込めなかったら NULL */

mCursor __mX11CursorLoad(const char *name)
{
	return XcursorLibraryLoadCursor(MLKX11_DISPLAY, name);
}

/** 1bit データからカーソル作成 */

mCursor __mX11CursorCreate1bit(const uint8_t *buf)
{
	Display *disp = MLKX11_DISPLAY;
	int w,h;
	Pixmap img,mask;
	XColor col[2];
	Cursor cur = 0;

	w = buf[0];
	h = buf[1];

	//Pixmap 作成

	img = XCreateBitmapFromData(disp, MLKX11_ROOTWINDOW,
			(char *)buf + 4, w, h);

	mask = XCreateBitmapFromData(disp, MLKX11_ROOTWINDOW,
			(char *)buf + 4 + ((w + 7) >> 3) * h, w, h);

	//カーソル作成

	if(img && mask)
	{
		col[0].pixel = 0;
		col[0].flags = DoRed | DoGreen | DoBlue;
		col[0].red = col[0].green = col[0].blue = 0;

		col[1].pixel = XWhitePixel(disp, MLKAPPX11->screen);
		col[1].flags = DoRed | DoGreen | DoBlue;
		col[1].red = col[1].green = col[1]. blue = 0xffff;

		cur = XCreatePixmapCursor(disp, img, mask, col, col + 1, buf[2], buf[3]);
	}

	//

	if(img) XFreePixmap(disp, img);
	if(mask) XFreePixmap(disp, mask);

	return cur;
}

/** RGBA イメージからカーソル作成 */

mCursor __mX11CursorCreateRGBA(int width,int height,int hotspot_x,int hotspot_y,const uint8_t *buf)
{
	XcursorImage *img;
	XcursorPixel *pd;
	int ix,iy;
	mCursor cur;

	//イメージ作成

	img = XcursorImageCreate(width, height);
	if(!img) return 0;

	img->xhot = hotspot_x;
	img->yhot = hotspot_y;

	//変換

	pd = img->pixels;

	for(iy = height; iy; iy--)
	{
		for(ix = width; ix; ix--, buf += 4)
		{
			*(pd++) = ((uint32_t)buf[3] << 24) | (buf[0] << 16) | (buf[1] << 8) | buf[2];
		}
	}

	//カーソルに変換

	cur = XcursorImageLoadCursor(MLKX11_DISPLAY, img);

	XcursorImageDestroy(img);

	return cur;
}

