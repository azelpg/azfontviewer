/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow [メインウィンドウ]
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_guicol.h"
#include "mlk_key.h"
#include "mlk_pixbuf.h"
#include "mlk_menubar.h"
#include "mlk_menu.h"
#include "mlk_label.h"
#include "mlk_button.h"
#include "mlk_checkbutton.h"
#include "mlk_arrowbutton.h"
#include "mlk_lineedit.h"
#include "mlk_listview.h"
#include "mlk_listheader.h"
#include "mlk_combobox.h"
#include "mlk_splitter.h"
#include "mlk_tab.h"
#include "mlk_sysdlg.h"
#include "mlk_str.h"
#include "mlk_imagelist.h"
#include "mlk_font.h"
#include "mlk_fontinfo.h"

#include "def_mainwin.h"
#include "def_appdata.h"
#include "trid.h"
#include "font.h"

#include "def_mainmenu.h"


//----------------------

#define _VERSION_TEXT  "azfontviewer ver 1.0.3\n\nCopyright (c) 2020-2022 Azel\n\nThis software is under the MIT License"
#define _SYSTEMDIR_NUM  3

static const char *g_sysdir_path[] = {
	"/usr/share/fonts", "~/.local/share/fonts", "~/.fonts"
};

//----------------------

static int _event_handle(mWidget *wg,mEvent *ev);

void MainWindow_notify_handle(MainWindow *p,mEventNotify *ev);

//----------------------


//=========================
// プレビュー
//=========================

#define _PREVHORZ_PADX  10
#define _PREVVERT_PADY  10
#define _PREVCOL_ASCENT   0xcccccc
#define _PREVCOL_BASELINE 0xdddddd


/* 横書きプレビュー、描画ハンドラ */

static void _prevhorz_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	mFont *font = APPDATA->font;
	int y,w,fonth;
	mPixCol col;

	mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h,
		mGuiCol_getPix(font? MGUICOL_WHITE: MGUICOL_FACE_DARK));
	
	if(font)
	{
		fonth = mFontGetHeight(font);
		y = (wg->h - fonth) / 2;
		w = wg->w - _PREVHORZ_PADX * 2;

		//[!] 筆記体などでは開始位置より左側にはみ出す場合があるため、左はクリッピングしない

		if(mPixbufClip_setBox_d(pixbuf, 0, 0, wg->w - _PREVHORZ_PADX, wg->h))
		{
			//高さ

			if(APPDATA->fview & VIEW_F_ASCENT)
			{
				col = mRGBtoPix(_PREVCOL_ASCENT);
				
				mPixbufLineH(pixbuf, _PREVHORZ_PADX, y - 1, w, col);
				mPixbufLineH(pixbuf, _PREVHORZ_PADX, y + fonth, w, col);
			}

			//baseline

			if(APPDATA->fview & VIEW_F_BASELINE)
				mPixbufLineH(pixbuf, _PREVHORZ_PADX, y + mFontGetAscender(font), w, mRGBtoPix(_PREVCOL_BASELINE));

			//テキスト
			
			mFontDrawText_pixbuf(font, pixbuf, _PREVHORZ_PADX, y,
				APPDATA->strSample.buf, APPDATA->strSample.len, 0);
		}
	}

	mPixbufClip_clear(pixbuf);
	mPixbufBox(pixbuf, 0, 0, wg->w, wg->h, 0);
}

/* 縦書きプレビュー、描画ハンドラ */

static void _prevvert_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	mFont *font = APPDATA->font;
	int x,h,half;
	uint32_t flags;
	mPixCol col;

	mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h,
		mGuiCol_getPix(font? MGUICOL_WHITE: MGUICOL_FACE_DARK));
	
	if(font)
	{
		half = mFontGetVertHeight(font) / 2;
		x = wg->w / 2;
		h = wg->h - _PREVVERT_PADY * 2;

		//[!] 上端のクリッピングは行わない

		if(mPixbufClip_setBox_d(pixbuf, 0, 0, wg->w, wg->h - _PREVVERT_PADY))
		{
			//高さ

			if(APPDATA->fview & VIEW_F_ASCENT)
			{
				col = mRGBtoPix(_PREVCOL_ASCENT);
				
				mPixbufLineV(pixbuf, x - half, _PREVVERT_PADY, h, col);
				mPixbufLineV(pixbuf, x + half, _PREVVERT_PADY, h, col);
			}

			//baseline

			if(APPDATA->fview & VIEW_F_BASELINE)
				mPixbufLineV(pixbuf, x, _PREVVERT_PADY, h, mRGBtoPix(_PREVCOL_BASELINE));

			//テキスト

			flags = 0;

			if(APPDATA->fview & VIEW_F_VORG) flags |= FONTDRAW_F_VORG;
			
			FontDrawText_vert(font, pixbuf, x, _PREVVERT_PADY,
				APPDATA->strSample.buf, APPDATA->strSample.len, 0, flags);
		}
	}

	mPixbufClip_clear(pixbuf);
	mPixbufBox(pixbuf, 0, 0, wg->w, wg->h, 0);
}


//===========================
// mSplitter
// (ファイルリストの次)
//===========================


/* ターゲット取得 */

static int _splitter_list_target(mSplitter *sp,mSplitterTarget *dst)
{
	mWidget *wg;

	dst->wgprev = sp->wg.prev;

	//next (常に EXPAND_W のウィジェット)

	for(wg = sp->wg.next; wg && !(wg->flayout & MLF_EXPAND_W); wg = wg->next);

	dst->wgnext = wg;

	//

	dst->prev_min = dst->next_min = 5;

	dst->prev_cur = dst->wgprev->w;
	dst->next_cur = dst->wgnext->w;
	
	dst->prev_max = dst->next_max = dst->prev_cur + dst->next_cur;

	return 1;
}


//==============================
// mListView (ファイルリスト)
//==============================


/* イベントハンドラ */

static int _filelist_event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_KEYDOWN && !ev->key.is_grab_pointer)
	{
		switch(ev->key.key)
		{
			//左 : 親へ移動
			case MKEY_LEFT:
			case MKEY_KP_LEFT:
				MainWindow_filelist_moveParent(APPDATA->mainwin);
				return 1;
			//右 : 現在のディレクトリへ移動
			case MKEY_RIGHT:
			case MKEY_KP_RIGHT:
				MainWindow_filelist_moveSelDir(APPDATA->mainwin);
				return 1;
		}
	}

	return mListViewHandle_event(wg, ev);
}


//===========================
// 作成・初期化
//===========================


/* ラベル+コンボボックスの作成 (ヌルで区切られた文字列) */

static void _create_label_combobox(mWidget *parent,int id_label,int id_combo,const char *text,int sel)
{
	mComboBox *cb;

	mLabelCreate(parent, MLF_MIDDLE, 0, 0, MLK_TR(id_label));

	//

	cb = mComboBoxCreate(parent, id_combo, 0, 0, 0);

	mComboBoxAddItems_sepnull(cb, text, 0);
	mComboBoxSetAutoWidth(cb);
	mComboBoxSetSelItem_atIndex(cb, sel);
}

/* プレビュー設定のタブ内容作成 */

static void _create_tab_prevopt(MainWindow *p,mWidget *parent)
{
	mWidget *ct_top,*ct,*ct2;
	mComboBox *cb;

	ct_top = mContainerCreateVert(parent, 6, MLF_EXPAND_WH, 0);

	p->wg_tab_top[1] = ct_top;

	mContainerSetPadding_pack4(MLK_CONTAINER(ct_top), MLK_MAKE32_4(6,6,0,0));

	//======= 上

	ct = mContainerCreateGrid(ct_top, 2, 8, 7, 0, MLK_MAKE32_4(0,0,0,5));

	//---- サイズ

	mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_WG_SIZE));

	ct2 = mContainerCreateHorz(ct, 6, 0, 0);

	//mLineEdit

	p->le_fontsize = mLineEditCreate(ct2, WID_EDIT_FONTSIZE, MLF_MIDDLE, 0,
		MLINEEDIT_S_SPIN | MLINEEDIT_S_NOTIFY_CHANGE);

	mLineEditSetWidth_textlen(p->le_fontsize, 7);
	mLineEditSetNumStatus(p->le_fontsize, 1, 100000, 1);
	mLineEditSetNum(p->le_fontsize, APPDATA->fontsize);
	mLineEditSetSpinValue(p->le_fontsize, 10);

	//mComboBox

	cb = mComboBoxCreate(ct2, WID_CB_FONTSIZE, MLF_MIDDLE, 0, 0);

	mComboBoxAddItem_static(cb, "pt", 0);
	mComboBoxAddItem_static(cb, "px", 1);

	mComboBoxSetSelItem_atIndex(cb, APPDATA->fontsize_unit);
	mComboBoxSetAutoWidth(cb);

	//---- ヒンティング

	_create_label_combobox(ct, TRID_WG_HINTING, WID_CB_HINTING,
		"none\0slight\0medium\0full\0", APPDATA->hinting);

	//---- 描画

	_create_label_combobox(ct, TRID_WG_RENDERING, WID_CB_RENDERING,
		"mono\0grayscale\0LCD (RGB)\0LCD (BGR)\0LCD-V (RGB)\0LCD-V (BGR)\0", APPDATA->rendering);

	//---- LCDフィルタ

	_create_label_combobox(ct, TRID_WG_LCD_FILTER, WID_CB_LCDFILTER,
		"none\0default\0light\0", APPDATA->lcdfilter);

	//===== 下 (フラグ)

	mCheckButtonCreate(ct_top, WID_CK_ENABLE_BITMAP, 0, 0, 0, MLK_TR(TRID_WG_ENABLE_BITMAP),
		APPDATA->fprevopt & PREVOPT_F_ENABLE_BITMAP);

	mCheckButtonCreate(ct_top, WID_CK_DISABLE_AUTOHINT, 0, 0, 0, MLK_TR(TRID_WG_DISABLE_AUTOHINT),
		APPDATA->fprevopt & PREVOPT_F_DISABLE_AUTOHINT);
}

/* フォント情報のタブ内容作成 */

static void _create_tab_fontinfo(MainWindow *p,mWidget *parent)
{
	mListView *list;
	mListHeader *lh;

	p->list_fontinfo = list = mListViewCreate(parent, WID_LIST_FONTINFO, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_MULTI_COLUMN | MLISTVIEW_S_GRID_COL,
		MSCROLLVIEW_S_HORZVERT_FRAME);

	p->wg_tab_top[0] = (mWidget *)list;

	//ヘッダ

	lh = mListViewGetHeaderWidget(list);

	mListHeaderAddItem(lh, NULL, 100, 0, 0);
	mListHeaderAddItem(lh, NULL, 100, MLISTHEADER_ITEM_F_EXPAND, 0);

	//リスト項目

	mListViewAddItem_text_static(list, MLK_TR(TRID_WG_FONTNAME));
	mListViewAddItem_text_static(list, MLK_TR(TRID_WG_STYLE));
	mListViewAddItem_text_static(list, MLK_TR(TRID_WG_VERSION));
	mListViewAddItem_text_static(list, "EM");
	mListViewAddItem_text_static(list, MLK_TR(TRID_WG_BITMAP));
	mListViewAddItem_text_static(list, MLK_TR(TRID_WG_VERT));
	mListViewAddItem_text_static(list, "CID-ROS");

	//

	mListViewSetColumnWidth_auto(list, 0);
}

/* 情報ウィジェット作成 */

static void _create_info_widgets(MainWindow *p,mWidget *parent)
{
	mWidget *ct_top,*ct,*ct2;
	mTab *tab;

	ct_top = mContainerCreateVert(parent, 7, MLF_EXPAND_WH, 0);

	mContainerSetPadding_pack4(MLK_CONTAINER(ct_top), MLK_MAKE32_4(6,6,0,0));

	//---- トップ

	ct = mContainerCreateGrid(ct_top, 2, 6, 7, MLF_EXPAND_W, 0);

	//サンプル文字列

	mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_WG_SAMPLE_TEXT));

	ct2 = mContainerCreateHorz(ct, 4, MLF_EXPAND_W, 0);

	p->le_sample = mLineEditCreate(ct2, WID_EDIT_SAMPLE, MLF_EXPAND_W | MLF_MIDDLE, 0, MLINEEDIT_S_NOTIFY_CHANGE);

	mLineEditSetText(p->le_sample, APPDATA->strSample.buf);

	mArrowButtonCreate(ct2, WID_BTT_SAMPLE_MENU, MLF_MIDDLE, 0, MARROWBUTTON_S_DOWN | MARROWBUTTON_S_FONTSIZE);

	//インデックス

	mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_WG_INDEX));

	p->cb_fontindex = mComboBoxCreate(ct, WID_CB_FONTINDEX, MLF_EXPAND_W, 0, 0);

	//--- タブ

	tab = mTabCreate(ct_top, WID_TAB_INFO, MLF_EXPAND_W, MLK_MAKE32_4(0,8,0,0), MTAB_S_TOP | MTAB_S_HAVE_SEP);

	mTabAddItem(tab, MLK_TR(TRID_WG_FONTINFO), -1, 0, 0);
	mTabAddItem(tab, MLK_TR(TRID_WG_PREV_OPTION), -1, 0, 0);

	mTabSetSel_atIndex(tab, APPDATA->tabno);

	//---- タブ内容

	_create_tab_fontinfo(p, ct_top);
	_create_tab_prevopt(p, ct_top);

	mWidgetShow(p->wg_tab_top[!APPDATA->tabno], 0);
}

/* ウィジェット作成 */

static void _create_widgets(MainWindow *p,WidgetConfig *conf)
{
	mWidget *wg,*ct,*ct1;

	MLK_TRGROUP(TRGROUP_ID_WIDGETS);

	//===== パス

	ct = mContainerCreateHorz(MLK_WIDGET(p), 4, MLF_EXPAND_W, 0);

	mContainerSetPadding_pack4(MLK_CONTAINER(ct), MLK_MAKE32_4(6,4,6,4));

	//mLineEdit

	p->le_dir = mLineEditCreate(ct, WID_EDIT_DIR, MLF_EXPAND_W | MLF_MIDDLE, 0, MLINEEDIT_S_NOTIFY_ENTER);

	//履歴ボタン

	mButtonCreate(ct, WID_BTT_RECENT, MLF_MIDDLE, 0, MBUTTON_S_REAL_W, MLK_TR(TRID_WG_RECENT));

	//===== 下

	ct1 = mContainerCreateHorz(MLK_WIDGET(p), 0, MLF_EXPAND_WH, 0);

	mContainerSetPadding_pack4(MLK_CONTAINER(ct1), MLK_MAKE32_4(6,0,6,6));

	//---- horz1: ファイルリスト

	ct = mContainerCreateVert(ct1, 5, MLF_FIX_W | MLF_EXPAND_H, 0);

	ct->w = conf->filelist_width;

	//リスト

	p->list = mListViewCreate(ct, WID_LIST, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_AUTO_WIDTH | MLISTVIEW_S_DESTROY_IMAGELIST,
		MSCROLLVIEW_S_HORZVERT_FRAME);

	p->list->wg.event = _filelist_event_handle;

	mWidgetSetFocus(MLK_WIDGET(p->list));

	mListViewSetImageList(p->list, mImageListLoadPNG("!/fileicon.png", 16));

	//フィルタ

	p->cb_filter = mComboBoxCreate(ct, WID_CB_FILTER, MLF_EXPAND_W, 0, 0);

	wg = (mWidget *)p->cb_filter;

	mComboBoxAddItem_static(MLK_COMBOBOX(wg), "All files", (intptr_t)"*\0");
	mComboBoxAddItem_static(MLK_COMBOBOX(wg), "TTF/OTF", (intptr_t)"*.ttf\0*.otf\0*.ttc\0*.otc\0");

	mComboBoxSetSelItem_atIndex(MLK_COMBOBOX(wg), conf->filter_index);

	//---- horz2: mSplitter

	wg = (mWidget *)mSplitterNew(ct1, 0, MSPLITTER_S_VERT);

	mSplitterSetFunc_getTarget(MLK_SPLITTER(wg), _splitter_list_target, 0);

	//---- horz3: 垂直プレビュー

	p->prev_vert = wg = mWidgetNew(ct1, 0);

	wg->flayout = MLF_FIX_W | MLF_EXPAND_H;
	wg->w = conf->prevvert_width;
	wg->draw = _prevvert_draw_handle;

	//---- horz4: mSplitter

	mSplitterNew(ct1, 0, MSPLITTER_S_VERT);

	//---- horz5: 水平プレビュー/情報

	ct = mContainerCreateVert(ct1, 0, MLF_EXPAND_WH, 0);

	//水平プレビュー

	p->prev_horz = wg = mWidgetNew(ct, 0);

	wg->flayout = MLF_EXPAND_W | MLF_FIX_H;
	wg->h = conf->prevhorz_height;
	wg->draw = _prevhorz_draw_handle;

	//mSplitter

	mSplitterNew(ct, 0, MSPLITTER_S_HORZ);

	//情報

	_create_info_widgets(p, ct);
}

/* メニュー作成 */

static void _create_menu(MainWindow *p)
{
	mMenuBar *bar;
	mMenu *menu,*menu_top;
	uint32_t f;
	int i;

	bar = mMenuBarNew(MLK_WIDGET(p), 0, 0);

	mToplevelAttachMenuBar(MLK_TOPLEVEL(p), bar);

	//データから項目をセット

	MLK_TRGROUP(TRGROUP_ID_MAINMENU);

	mMenuBarCreateMenuTrArray16(bar, g_mainmenu_data);

	menu_top = mMenuBarGetMenu(bar);

	//システムディレクトリ

	menu = mMenuNew();

	for(i = 0; i < _SYSTEMDIR_NUM; i++)
		mMenuAppendText(menu, MAINWIN_CMDID_SYSDIR_TOP + i, g_sysdir_path[i]);

	mMenuSetItemSubmenu(menu_top, TRID_MENU_FILE_SYSTEMDIR, menu);

	//履歴サブメニュー

	p->menu_recent = mMenuNew();

	mMenuAppendStrArray(p->menu_recent, APPDATA->strRecentDir, MAINWIN_CMDID_RECENT_DIR, RECENT_NUM);

	mMenuSetItemSubmenu(menu_top, TRID_MENU_FILE_RECENT, p->menu_recent);

	//初期チェック

	menu = mMenuGetItemSubmenu(menu_top, TRID_MENU_TOP_VIEW);

	f = APPDATA->fview;

	mMenuSetItemCheck(menu, TRID_MENU_VIEW_PREV_HORZ, f & VIEW_F_PREV_HORZ);
	mMenuSetItemCheck(menu, TRID_MENU_VIEW_PREV_VERT, f & VIEW_F_PREV_VERT);
	mMenuSetItemCheck(menu, TRID_MENU_VIEW_ASCENT, f & VIEW_F_ASCENT);
	mMenuSetItemCheck(menu, TRID_MENU_VIEW_BASELINE, f & VIEW_F_BASELINE);
	mMenuSetItemCheck(menu, TRID_MENU_VIEW_VORG, f & VIEW_F_VORG);
}

/** メインウィンドウ作成・表示 */

void MainWindowNew(WidgetConfig *conf)
{
	MainWindow *p;

	//ウィンドウ
	
	p = (MainWindow *)mToplevelNew(NULL, sizeof(MainWindow),
			MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_TAB_MOVE);
	if(!p) return;
	
	APPDATA->mainwin = p;

	p->wg.fstate |= MWIDGET_STATE_ENABLE_DROP;
	p->wg.event = _event_handle;

	//タイトル

	mToplevelSetTitle(MLK_TOPLEVEL(p), "azfontviewer");

	//アイコン

	mToplevelSetIconPNG_file(MLK_TOPLEVEL(p), "!/appicon.png");

	//メニュー

	_create_menu(p);

	//ウィジェット

	_create_widgets(p, conf);

	//プレビューの非表示

	if(!(APPDATA->fview & VIEW_F_PREV_HORZ))
	{
		mWidgetShow(p->prev_horz, 0);
		mWidgetShow(p->prev_horz->next, 0);
	}

	if(!(APPDATA->fview & VIEW_F_PREV_VERT))
	{
		mWidgetShow(p->prev_vert, 0);
		mWidgetShow(p->prev_vert->next, 0);
	}

	//

	MainWindow_onChangePath(p);

	//状態復元

	if(!conf->winst.w || !conf->winst.h)
	{
		conf->winst.w = 500;
		conf->winst.h = 400;
	}

	mToplevelSetSaveState(MLK_TOPLEVEL(p), &conf->winst);

	//表示

	mWidgetShow(MLK_WIDGET(p), 1);
}

/** 設定保存用のデータ取得 */

void MainWindow_getConfig(MainWindow *p,WidgetConfig *cf)
{
	mToplevelGetSaveState(MLK_TOPLEVEL(p), &cf->winst);

	cf->filelist_width = p->list->wg.w;
	cf->prevhorz_height = p->prev_horz->h;
	cf->prevvert_width = p->prev_vert->w;

	cf->filter_index = mComboBoxGetSelIndex(p->cb_filter);
}


//===========================
// ハンドラ
//===========================


/* ディレクトリを開く */

static void _cmd_opendir(MainWindow *p)
{
	if(mSysDlg_selectDir(MLK_WINDOW(p), APPDATA->strPath.buf, 0, &APPDATA->strPath))
		MainWindow_onChangePath(p);
}

/* システムディレクトリを開く */

static void _cmd_open_sysdir(MainWindow *p,const char *path)
{
	if(path[0] == '~')
		mStrPathSetHome_join(&APPDATA->strPath, path + 2);
	else
		mStrSetText(&APPDATA->strPath, path);

	MainWindow_onChangePath(p);
}


/* COMMAND イベント */

static void _event_command(MainWindow *p,mEvent *ev)
{
	int id = ev->cmd.id,n;

	//履歴

	if(id >= MAINWIN_CMDID_RECENT_DIR && id < MAINWIN_CMDID_RECENT_DIR + RECENT_NUM)
	{
		mStrCopy(&APPDATA->strPath, APPDATA->strRecentDir + (id - MAINWIN_CMDID_RECENT_DIR));
		MainWindow_onChangePath(p);
		return;
	}

	//システムディレクトリ

	if(id >= MAINWIN_CMDID_SYSDIR_TOP && id < MAINWIN_CMDID_SYSDIR_TOP + _SYSTEMDIR_NUM)
	{
		_cmd_open_sysdir(p, g_sysdir_path[id - MAINWIN_CMDID_SYSDIR_TOP]);
		return;
	}

	switch(id)
	{
		//---- ファイル

		//ディレクトリを開く
		case TRID_MENU_FILE_OPENDIR:
			_cmd_opendir(p);
			break;
		//終了
		case TRID_MENU_FILE_EXIT:
			mGuiQuit();
			break;

		//---- 表示

		//横書きプレビュー
		case TRID_MENU_VIEW_PREV_HORZ:
			APPDATA->fview ^= VIEW_F_PREV_HORZ;
			n = ((APPDATA->fview & VIEW_F_PREV_HORZ) != 0);
			
			mWidgetShow(p->prev_horz, n);
			mWidgetShow(p->prev_horz->next, n);
			mWidgetLayout(MLK_WIDGET(p));
			break;
		//縦書きプレビュー
		case TRID_MENU_VIEW_PREV_VERT:
			APPDATA->fview ^= VIEW_F_PREV_VERT;
			
			n = ((APPDATA->fview & VIEW_F_PREV_VERT) != 0);
			
			mWidgetShow(p->prev_vert, n);
			mWidgetShow(p->prev_vert->next, n);
			mWidgetLayout(MLK_WIDGET(p));
			break;
		//高さ
		case TRID_MENU_VIEW_ASCENT:
			APPDATA->fview ^= VIEW_F_ASCENT;
			MainWindow_updatePreview(p);
			break;
		//ベースライン
		case TRID_MENU_VIEW_BASELINE:
			APPDATA->fview ^= VIEW_F_BASELINE;
			MainWindow_updatePreview(p);
			break;
		//VORG 適用
		case TRID_MENU_VIEW_VORG:
			APPDATA->fview ^= VIEW_F_VORG;
			mWidgetRedraw(p->prev_vert);
			break;

		//---- ヘルプ

		//Tips
		case TRID_MENU_HELP_TIPS:
			mMessageBoxOK(MLK_WINDOW(p), MLK_TR2(TRGROUP_ID_WIDGETS, TRID_WG_TIPS_MESSAGE));
			break;
		//バージョン情報
		case TRID_MENU_HELP_VERSION:
			mSysDlg_about(MLK_WINDOW(p), _VERSION_TEXT);
			break;
	}
}

/* イベントハンドラ */

int _event_handle(mWidget *wg,mEvent *ev)
{
	switch(ev->type)
	{
		//通知
		case MEVENT_NOTIFY:
			MainWindow_notify_handle((MainWindow *)wg, (mEventNotify *)ev);
			break;
		//コマンド
		case MEVENT_COMMAND:
			_event_command((MainWindow *)wg, ev);
			break;
		//Drop
		case MEVENT_DROP_FILES:
			MainWindow_setFromFile((MainWindow *)wg, *(ev->dropfiles.files));
			break;
	
		//閉じるボタン
		case MEVENT_CLOSE:
			mGuiQuit();
			break;
		
		default:
			return FALSE;
	}

	return TRUE;
}

