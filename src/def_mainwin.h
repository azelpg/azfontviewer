/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * MainWindow 定義
 **********************************/

typedef struct _MainWindow
{
	MLK_TOPLEVEL_DEF

	mMenu *menu_recent;	//履歴メニュー

	mLineEdit *le_dir,
		*le_sample,
		*le_fontsize;
	mListView *list,
		*list_fontinfo;
	mComboBox *cb_filter,
		*cb_fontindex;
	mWidget *prev_horz,
		*prev_vert,
		*wg_tab_top[2];
}MainWindow;

/* メニューコマンドID */
enum
{
	MAINWIN_CMDID_SYSDIR_TOP = 10000,	//システムディレクトリ
	MAINWIN_CMDID_RECENT_DIR = 11000	//履歴
};

/* ウィジェットID */
enum
{
	WID_EDIT_DIR = 100,
	WID_BTT_RECENT,
	WID_LIST,
	WID_CB_FILTER,
	WID_EDIT_SAMPLE,
	WID_BTT_SAMPLE_MENU,
	WID_CB_FONTINDEX,
	WID_TAB_INFO,
	WID_LIST_FONTINFO,
	WID_EDIT_FONTSIZE,
	WID_CB_FONTSIZE,
	WID_CB_HINTING,
	WID_CB_RENDERING,
	WID_CB_LCDFILTER,
	WID_CK_ENABLE_BITMAP,
	WID_CK_DISABLE_AUTOHINT
};


void MainWindow_setFileList(MainWindow *p);
void MainWindow_onChangePath(MainWindow *p);
void MainWindow_filelist_moveParent(MainWindow *p);
void MainWindow_filelist_moveSelDir(MainWindow *p);
void MainWindow_setFromFile(MainWindow *p,const char *filename);

void MainWindow_setFontFile(MainWindow *p,const char *fname,int index);
void MainWindow_updatePreview(MainWindow *p);
