/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * 翻訳文字列ID
 **********************************/

/* グループ */
enum
{
	TRGROUP_ID_WIDGETS = 0,
	TRGROUP_ID_DLG_SAMPLE,
	TRGROUP_ID_SAMPLE_MENU,
	TRGROUP_ID_MAINMENU = 1000
};

/* ウィジェット */
enum
{
	TRID_WG_RECENT = 0,
	TRID_WG_SAMPLE_TEXT,
	TRID_WG_INDEX,
	TRID_WG_FONTINFO,
	TRID_WG_PREV_OPTION,
	TRID_WG_FONTNAME,
	TRID_WG_STYLE,
	TRID_WG_VERSION,
	TRID_WG_BITMAP,
	TRID_WG_VERT,
	TRID_WG_SIZE,
	TRID_WG_HINTING,
	TRID_WG_RENDERING,
	TRID_WG_LCD_FILTER,
	TRID_WG_ENABLE_BITMAP,
	TRID_WG_DISABLE_AUTOHINT,
	TRID_WG_TIPS_MESSAGE
};

/* サンプルメニュー */
enum
{
	TRID_SAMPLEMENU_ADD = 0,
	TRID_SAMPLEMENU_EDIT,

	TRID_SAMPLEMENU_NAME = 100
};

/* メニュー */
enum
{
	TRID_MENU_TOP_FILE = 1,
	TRID_MENU_TOP_VIEW,
	TRID_MENU_TOP_HELP,

	TRID_MENU_FILE_OPENDIR = 1000,
	TRID_MENU_FILE_SYSTEMDIR,
	TRID_MENU_FILE_RECENT,
	TRID_MENU_FILE_EXIT,

	TRID_MENU_VIEW_PREV_HORZ = 1100,
	TRID_MENU_VIEW_PREV_VERT,
	TRID_MENU_VIEW_ASCENT,
	TRID_MENU_VIEW_BASELINE,
	TRID_MENU_VIEW_VORG,

	TRID_MENU_HELP_TIPS = 1200,
	TRID_MENU_HELP_VERSION
};
