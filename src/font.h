/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント
 **********************************/

enum
{
	FONTDRAW_F_VORG = 1<<0	//VORG 適用
};

mFont *FontCreate(mFontSystem *sys,mFontInfo *info);

const char *FontGetName(mFont *p);
const char *FontGetStyleName(mFont *p);
int FontGetEM(mFont *p);
const char *FontGetHaveBitmap(mFont *p);
void FontGetVert(mFont *p,mStr *str);
void FontGetCID_ROS(mFont *p,mStr *str);
void FontGetVersionText(mFont *p,mStr *str);

void FontDrawText_vert(mFont *p,mPixbuf *img,int x,int y,const char *text,int len,mRgbCol col,uint32_t flags);
