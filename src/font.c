/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * フォント
 *****************************************/

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H
#include FT_CID_H

#define MLK_FONT_FREETYPE_DEFINE

#include "mlk.h"
#include "mlk_font.h"
#include "mlk_font_freetype.h"
#include "mlk_opentype.h"
#include "mlk_pixbuf.h"
#include "mlk_unicode.h"
#include "mlk_str.h"

#include "font.h"


//----------------------

//フォントのサブデータ

struct _mFTSubData
{
	mOT_VORG vorg;		//縦書き原点位置
	uint8_t *gsub_vert;	//縦書き置き換え
	uint32_t flags;
};

enum
{
	SUBFLAGS_GSUB_VERT = 1<<0,
	SUBFLAGS_GSUB_VRT2 = 1<<1
};

//----------------------



//==============================
// OpenType 各データ読み込み
//==============================


/* VORG 読み込み */

static void _read_VORG(mFont *p)
{
	void *buf;
	uint32_t size;

	if(mFontFT_loadTable(p, MLK_MAKE32_4('V','O','R','G'), &buf, &size) == MLKERR_OK)
	{
		mOpenType_readVORG(buf, size, &p->sub->vorg);

		mFree(buf);
	}
}

/* GSUB: データ取得
 *
 * return: feature が存在するか */

static mlkbool _get_gsub_data(mOTLayout *p,uint32_t feature_tag,uint8_t **ppdst)
{
	mOT_TABLE tbl;
	uint32_t script_tag,
	 tags[] = {
		MLK_MAKE32_4('h','a','n','i'), MLK_MAKE32_4('k','a','n','a'), MLK_MAKE32_4('h','a','n','g'), 0
	};
	mlkbool ret = FALSE;

	mOTLayout_searchScriptList(p, tags, &script_tag);

	if(mOTLayout_getFeature2(p, script_tag, 0, feature_tag, &tbl) == MLKERR_OK)
	{
		ret = TRUE;

		mOTLayout_createGSUB_single(p, &tbl, ppdst);
	}

	return ret;
}

/* GSUB 読み込み */

static void _read_GSUB(mFont *p)
{
	mOTLayout *ot;
	void *buf;
	uint8_t *vert = NULL,*vrt2 = NULL;
	uint32_t size;

	if(mFontFT_loadTable(p, MLK_MAKE32_4('G','S','U','B'), &buf, &size))
		return;

	if(mOTLayout_new(&ot, buf, size, FALSE) == MLKERR_OK)
	{
		//vert

		if(_get_gsub_data(ot, MOPENTYPE_FEATURE_VERT, &vert))
			p->sub->flags |= SUBFLAGS_GSUB_VERT;

		//vrt2
		
		if(_get_gsub_data(ot, MOPENTYPE_FEATURE_VRT2, &vrt2))
			p->sub->flags |= SUBFLAGS_GSUB_VRT2;

		//結合
		// :フォントによっては、両方使わなければ正しく置き換えられない場合があるため、
		// :両方のデータを結合する。

		p->sub->gsub_vert = mOpenType_combineData_GSUB_single(vert, vrt2);
	
		mOTLayout_free(ot);
	}

	mFree(buf);
}


//==============================
// main
//==============================


/* フォント解放時ハンドラ */

static void _free_handle(mFont *font)
{
	mFTSubData *p = font->sub;

	//サブデータ

	if(p)
	{
		mFree(p->vorg.buf);
		mFree(p->gsub_vert);
		
		mFree(p);
	}
}

/** フォント作成 */

mFont *FontCreate(mFontSystem *sys,mFontInfo *info)
{
	mFont *p;

	//作成
	
	p = mFontCreate_raw(sys, info);
	if(!p) return NULL;

	//サブデータ

	p->sub = (mFTSubData *)mMalloc0(sizeof(mFTSubData));
	if(!p->sub)
	{
		mFontFree(p);
		return NULL;
	}

	p->free_font = _free_handle;

	//

	_read_VORG(p);
	_read_GSUB(p);
	
	return p;
}

/** フォント名取得 */

const char *FontGetName(mFont *p)
{
	return (p)? p->face->family_name: NULL;
}

/** スタイル名取得 */

const char *FontGetStyleName(mFont *p)
{
	return (p)? p->face->style_name: NULL;
}

/** EM 値取得 */

int FontGetEM(mFont *p)
{
	return (p)? p->face->units_per_EM: 0;
}

/** 埋め込みビットマップがあるか */

const char *FontGetHaveBitmap(mFont *p)
{
	if(!p)
		return NULL;
	else
		return (p->face->face_flags & FT_FACE_FLAG_FIXED_SIZES)? "o": "x";
}

/** 縦書きの情報取得 */

void FontGetVert(mFont *p,mStr *str)
{
	if(!p)
		mStrEmpty(str);
	else
	{
		mStrSetChar(str, (p->face->face_flags & FT_FACE_FLAG_VERTICAL)? 'o': 'x');

		if(p->sub->vorg.have_vorg)
			mStrAppendText(str, " [VORG]");

		if(p->sub->flags & SUBFLAGS_GSUB_VERT)
			mStrAppendText(str, " [GSUB-vert]");

		if(p->sub->flags & SUBFLAGS_GSUB_VRT2)
			mStrAppendText(str, " [GSUB-vrt2]");
	}
}

/** CID ROS 取得 */

void FontGetCID_ROS(mFont *p,mStr *str)
{
	const char *reg,*ord;
	int sup;

	mStrEmpty(str);

	if(p && FT_Get_CID_Registry_Ordering_Supplement(p->face, &reg, &ord, &sup) == 0)
	{
		mStrSetFormat(str, "%s-%s-%d", reg, ord, sup);
	}
}

/** バージョン文字列を取得 */

void FontGetVersionText(mFont *p,mStr *str)
{
	mStrEmpty(str);

	if(p)
		mFontFT_getNameTbl_id(p->face, 5, str);
}


//=================================
// 縦書き用
//=================================


/* [縦書き] GID から、ビットマップグリフをロード */

static mlkbool _loadglyph_vert_gid(mFont *p,uint32_t gid,uint32_t flags,mFTPos *pos)
{
	FT_Face face = p->face;
	int origin;

	//グリフスロットにロード

	if(FT_Load_Glyph(face, gid, p->gdraw.fload_glyph | FT_LOAD_VERTICAL_LAYOUT))
		return FALSE;

	//

	if(face->glyph->format == FT_GLYPH_FORMAT_OUTLINE)
	{
		//------ アウトライン

		//Y 原点
		// :VORG テーブルがあればそこから取得。なければ ascender。

		origin = face->ascender;

		if(flags & FONTDRAW_F_VORG)
			mOpenType_getVORG_originY(&p->sub->vorg, gid, &origin);

		//原点を中央・上へ移動
		
		FT_Outline_Translate(&face->glyph->outline,
			FT_MulFix(-face->units_per_EM / 2, face->size->metrics.x_scale),
			FT_MulFix(-origin, face->size->metrics.y_scale));

		//ビットマップに変換

		if(FT_Render_Glyph(face->glyph, p->gdraw.render_mode))
			return FALSE;

		//

		pos->x = face->glyph->bitmap_left;
		pos->y = -(face->glyph->bitmap_top);
	}
	else if(face->glyph->format == FT_GLYPH_FORMAT_BITMAP)
	{
		//------ ビットマップ

		//[!] グリフ中央・上が原点になっている

		pos->x = face->glyph->bitmap_left;
		pos->y = face->glyph->bitmap_top;
	}

	//送り幅

	pos->advance = mFontFT_round_fix6(face->glyph->advance.y);
	
	return TRUE;
}

/* [縦書き] グリフ描画 (文字コード)
 *
 * return: 次の y 位置 */

static int _drawglyph_vert(mFont *p,int x,int y,uint32_t code,uint32_t flags,
	mFontDrawInfo *info,void *param)
{
	mFTPos pos;

	code = FT_Get_Char_Index(p->face, code);
	code = mOpenType_getGSUB_replaceSingle(p->sub->gsub_vert, code);

	//

	if(_loadglyph_vert_gid(p, code, flags, &pos))
	{
		mFontFT_drawGlyph(p, x + pos.x, y + pos.y, info, param);

		y += pos.advance;
	}

	return y;
}

/** 縦書きテキスト描画
 *
 * x,y: グリフの中央・上が原点
 * len: 常にセット */

void FontDrawText_vert(mFont *p,mPixbuf *img,int x,int y,
	const char *text,int len,mRgbCol col,uint32_t flags)
{
	mFontDrawInfo *info;
	mFontDrawParam_pixbuf param;
	const char *pc;
	uint32_t code;
	int ret;

	if(!p || !img || !text) return;
	
	//mPixbuf 描画用情報

	info = mFontGetDrawInfo_pixbuf();

	mFontSetDrawParam_pixbuf(&param, img, col);

	//

	mFontFT_setLCDFilter(p);

	for(pc = text; *pc && pc - text < len; )
	{
		ret = mUTF8GetChar(&code, pc, len - (pc - text), &pc);
		if(ret < 0) break;
		else if(ret > 0) continue;
		
		y = _drawglyph_vert(p, x, y, code, flags, info, &param);
	}
}

