/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * main
 **********************************/

#include <stdio.h>
#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_window.h"
#include "mlk_iniread.h"
#include "mlk_iniwrite.h"
#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_font.h"
#include "mlk_fontinfo.h"

#include "def_appdata.h"

#include "deftrans.h"


//----------------------

AppData *g_appdata = NULL;

#define CONFIG_FILENAME  "main.conf"

#define _HELP_TEXT "[usage] exe\n\n--help-mlk : show mlk options"

//----------------------

void MainWindowNew(WidgetConfig *conf);
void MainWindow_getConfig(MainWindow *p,WidgetConfig *cf);

//----------------------


//===========================
// 設定ファイル書き込み
//===========================


/* ウィンドウ状態書き込み */

static void _save_config_winstate(FILE *fp,const char *key,mToplevelSaveState *st)
{
	int32_t n[7];

	n[0] = st->x;
	n[1] = st->y;
	n[2] = st->w;
	n[3] = st->h;
	n[4] = st->norm_x;
	n[5] = st->norm_y;
	n[6] = st->flags;

	mIniWrite_putNumbers(fp, key, n, 7, 4, FALSE);
}

/* サンプルリスト書き込み */

static void _save_config_sample(FILE *fp,mList *list)
{
	SampleItem *si;
	mStr str = MSTR_INIT;
	int no = 0;

	MLK_LIST_FOR(*list, si, SampleItem)
	{
		mStrSetFormat(&str, "%s\t%s", si->name, si->text);
		
		mIniWrite_putText_keyno(fp, no++, str.buf);
	}

	mStrFree(&str);
}

/* 設定ファイル書き込み */

static void _save_config(void)
{
	FILE *fp;
	AppData *p = APPDATA;
	WidgetConfig conf;

	MainWindow_getConfig(p->mainwin, &conf);

	fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), CONFIG_FILENAME);
	if(!fp) return;

	//-------- メイン

	mIniWrite_putGroup(fp, "main");

	mIniWrite_putInt(fp, "ver", 1);

	_save_config_winstate(fp, "mainwin", &conf.winst);

	mIniWrite_putInt(fp, "filelist_w", conf.filelist_width);
	mIniWrite_putInt(fp, "prevhorz_h", conf.prevhorz_height);
	mIniWrite_putInt(fp, "prevvert_w", conf.prevvert_width);
	mIniWrite_putInt(fp, "filter_index", conf.filter_index);

	mIniWrite_putStr(fp, "sample", &p->strSample);
	mIniWrite_putStr(fp, "path", &p->strPath);

	mIniWrite_putInt(fp, "tabno", p->tabno);
	mIniWrite_putInt(fp, "fontsize", p->fontsize);
	mIniWrite_putInt(fp, "fontsize_unit", p->fontsize_unit);
	mIniWrite_putInt(fp, "hinting", p->hinting);
	mIniWrite_putInt(fp, "rendering", p->rendering);
	mIniWrite_putInt(fp, "lcdfilter", p->lcdfilter);

	mIniWrite_putHex(fp, "fview", p->fview);
	mIniWrite_putHex(fp, "fprevopt", p->fprevopt);

	//------ ファイル履歴

	mIniWrite_putGroup(fp, "recentdir");
	mIniWrite_putStrArray(fp, 0, p->strRecentDir, RECENT_NUM);

	//----- サンプル

	mIniWrite_putGroup(fp, "sample");

	_save_config_sample(fp, &p->list_sample);

	//------ mlk

	mGuiWriteIni_system(fp);

	fclose(fp);
}


//===========================
// 設定ファイル読み込み
//===========================


/* ウィンドウ状態読み込み */

static void _read_config_winstate(mIniRead *ini,const char *key,mToplevelSaveState *state)
{
	int32_t n[7];

	//[!] state はゼロクリアされているので、初期値は設定しなくてよい

	if(mIniRead_getNumbers(ini, key, n, 7, 4, FALSE) == 7)
	{
		state->x = n[0];
		state->y = n[1];
		state->w = n[2];
		state->h = n[3];
		state->norm_x = n[4];
		state->norm_y = n[5];
		state->flags = n[6];
	}
}

/* サンプルリスト読み込み */

static void _read_config_sample(mIniRead *ini,mList *list)
{
	SampleItem *si;
	const char *key,*param;

	while(mIniRead_getNextItem(ini, &key, &param))
	{
		key = strchr(param, '\t');

		if(key)
		{
			si = (SampleItem *)mListAppendNew(list, sizeof(SampleItem));
			if(si)
			{
				si->name = mStrndup(param, key - param);
				si->text = mStrdup(key + 1);
			}
		}
	}
}

/* 設定ファイル読み込み */

static void _read_config(WidgetConfig *cf)
{
	AppData *p = APPDATA;
	mIniRead *ini;

	mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), CONFIG_FILENAME);
	if(!ini) return;

	//------- main

	mIniRead_setGroup(ini, "main");

	//バージョンが1以外なら、空にする

	if(mIniRead_getInt(ini, "ver", 0) != 1)
		mIniRead_setEmpty(ini);

	//ウィジェット

	_read_config_winstate(ini, "mainwin", &cf->winst);

	cf->filelist_width = mIniRead_getInt(ini, "filelist_w", 200);
	cf->prevhorz_height = mIniRead_getInt(ini, "prevhorz_h", 60);
	cf->prevvert_width = mIniRead_getInt(ini, "prevvert_w", 70);
	cf->filter_index = mIniRead_getInt(ini, "filter_index", 0);

	//

	mIniRead_getTextStr(ini, "sample", &p->strSample, "sample");
	mIniRead_getTextStr(ini, "path", &p->strPath, NULL);

	if(mStrIsEmpty(&p->strPath))
		mStrSetText(&p->strPath, "/usr/share/fonts");

	//

	p->tabno = mIniRead_getInt(ini, "tabno", 0);
	p->fontsize = mIniRead_getInt(ini, "fontsize", 130);
	p->fontsize_unit = mIniRead_getInt(ini, "fontsize_unit", 0);
	p->hinting = mIniRead_getInt(ini, "hinting", 0);
	p->rendering = mIniRead_getInt(ini, "rendering", 1);
	p->lcdfilter = mIniRead_getInt(ini, "lcdfilter", 1);

	p->fview = mIniRead_getHex(ini, "fview",
		VIEW_F_PREV_HORZ | VIEW_F_PREV_VERT | VIEW_F_ASCENT | VIEW_F_VORG);
	
	p->fprevopt = mIniRead_getHex(ini, "fprevopt", 0);

	//-------- ファイル履歴

	mIniRead_setGroup(ini, "recentdir");
	mIniRead_getTextStrArray(ini, 0, p->strRecentDir, RECENT_NUM);

	//---- サンプルリスト

	mIniRead_setGroup(ini, "sample");

	_read_config_sample(ini, &p->list_sample);

	//---- mlk 情報

	mGuiReadIni_system(ini);

	//

	mIniRead_end(ini);
}


//===========================
// 初期化
//===========================


/* フォント情報の初期状態をセット */

static void _set_fontinfo_init(AppData *p)
{
	mFontInfo *info = &p->fontinfo;

	//マスク

	info->mask = MFONTINFO_MASK_FILE | MFONTINFO_MASK_SIZE | MFONTINFO_MASK_EX;

	info->ex.mask = MFONTINFO_EX_MASK_HINTING | MFONTINFO_EX_MASK_RENDERING
		| MFONTINFO_EX_MASK_LCD_FILTER | MFONTINFO_EX_MASK_AUTO_HINT
		| MFONTINFO_EX_MASK_EMBEDDED_BITMAP;

	//サイズ

	info->size = p->fontsize / 10.0;

	if(p->fontsize_unit == 1)
		info->size = -info->size;

	//EX

	info->ex.hinting = p->hinting;
	info->ex.rendering = p->rendering;
	info->ex.lcd_filter = p->lcdfilter;

	if(!(p->fprevopt & PREVOPT_F_DISABLE_AUTOHINT))
		info->ex.flags |= MFONTINFO_EX_FLAGS_AUTO_HINT;

	if(p->fprevopt & PREVOPT_F_ENABLE_BITMAP)
		info->ex.flags |= MFONTINFO_EX_FLAGS_EMBEDDED_BITMAP;
}

/* サンプルのリストアイテム破棄ハンドラ */

static void _sampleitem_destroy_handle(mList *list,mListItem *item)
{
	mFree(((SampleItem *)item)->name);
	mFree(((SampleItem *)item)->text);
}

/** 初期化 */

static int _init(int argc,char **argv)
{
	WidgetConfig conf;

	//設定ファイルディレクトリ作成

	mGuiCreateConfigDir(NULL);

	//AppData 確保

	g_appdata = (AppData *)mMalloc0(sizeof(AppData));
	if(!g_appdata) return 1;

	APPDATA->list_sample.item_destroy = _sampleitem_destroy_handle;

	//設定ファイル読み込み

	mMemset0(&conf, sizeof(WidgetConfig));

	_read_config(&conf);

	//初期化

	_set_fontinfo_init(APPDATA);

	//メインウィンドウ作成・表示

	MainWindowNew(&conf);

	return 0;
}


//===========================
// メイン
//===========================


/* AppData 解放 */

static void _free_appdata(AppData *p)
{
	mFontFree(p->font);
	mFontInfoFree(&p->fontinfo);

	mListDeleteAll(&p->list_sample);

	mStrFree(&p->strSample);
	mStrFree(&p->strPath);
	mStrArrayFree(p->strRecentDir, RECENT_NUM);

	mFree(p);
}

/** 終了処理 */

static void _finish(void)
{
	//設定ファイル保存

	_save_config();

	//AppData 解放

	_free_appdata(APPDATA);
}

/** 初期化メイン */

static int _init_main(int argc,char **argv)
{
	int top,i;

	if(mGuiInit(argc, argv, &top)) return 1;

	//"--help"

	for(i = top; i < argc; i++)
	{
		if(strcmp(argv[i], "--help") == 0)
		{
			puts(_HELP_TEXT);
			mGuiEnd();
			return 1;
		}
	}

	//

	mGuiSetWMClass("azfontviewer", "azfontviewer");

	//パスセット

	mGuiSetPath_data_exe("../share/azfontviewer");
	mGuiSetPath_config_home(".config/azfontviewer");

	//翻訳データ

	mGuiLoadTranslation(g_deftransdat, NULL, "tr");

	//バックエンド初期化

	if(mGuiInitBackend()) return 1;

	//初期化

	if(_init(argc, argv))
	{
		mError("failed initialize\n");
		return 1;
	}

	return 0;
}

/** メイン */

int main(int argc,char **argv)
{
	//初期化
	
	if(_init_main(argc, argv)) return 1;

	//実行

	mGuiRun();

	//終了

	_finish();

	mGuiEnd();

	return 0;
}
