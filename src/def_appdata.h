/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * アプリデータ
 **********************************/

#include <mlk_fontinfo.h>

typedef struct _MainWindow MainWindow;

#define RECENT_NUM 10  //履歴の数


//-------------------
// AppData

typedef struct
{
	MainWindow *mainwin;

	mFont *font;
	mFontInfo fontinfo;	//現在のフォント情報

	mList list_sample;	//サンプル文字列リスト

	int tabno,			//現在の情報のタブ
		fontsize,		//フォントサイズ
		fontsize_unit,	//フォントサイズ単位 (0:pt, 1:px)
		hinting,		//ヒンティング
		rendering,		//描画タイプ
		lcdfilter;		//LCDフィルタ
	uint32_t fview,		//表示フラグ
		fprevopt;		//プレビュー設定フラグ
	mStr strSample,		//サンプル文字列
		strPath,		//現在のパス
		strRecentDir[RECENT_NUM];	//ディレクトリ履歴
}AppData;


extern AppData *g_appdata;
#define APPDATA g_appdata


/* 表示フラグ */
enum
{
	VIEW_F_PREV_HORZ = 1<<0,	//横書きプレビュー
	VIEW_F_PREV_VERT = 1<<1,	//縦書きプレビュー
	VIEW_F_ASCENT = 1<<2,		//ascent/descent
	VIEW_F_BASELINE = 1<<3,		//ベースライン
	VIEW_F_VORG = 1<<4			//VORG 適用
};

/* プレビュー設定フラグ */
enum
{
	PREVOPT_F_ENABLE_BITMAP = 1<<0,		//ビットマップ有効
	PREVOPT_F_DISABLE_AUTOHINT = 1<<1	//オートヒント無効
};


//-------------------
// WidgetConfig

typedef struct
{
	mToplevelSaveState winst;	//メインウィンドウの状態
	int filelist_width,			//ファイルリストの幅
		prevhorz_height,
		prevvert_width,
		filter_index;		//ファイルリストのフィルタ
}WidgetConfig;


//-------------------------
// サンプル文字列アイテム

typedef struct
{
	mListItem i;
	char *name,*text;
}SampleItem;

