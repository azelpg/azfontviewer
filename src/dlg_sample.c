/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * サンプル編集ダイアログ
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_listview.h"
#include "mlk_label.h"
#include "mlk_lineedit.h"
#include "mlk_iconbar.h"
#include "mlk_columnitem.h"
#include "mlk_event.h"
#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_imagelist.h"

#include "def_appdata.h"
#include "trid.h"


//-------------------

typedef struct
{
	MLK_DIALOG_DEF

	mListView *list;
}_dialog;

typedef struct
{
	MLK_DIALOG_DEF

	mLineEdit *le_name,
		*le_text;
}_dialog_edit;

#define WID_LIST 100

enum
{
	TRID_TITLE,
	TRID_NAME,
	TRID_TEXT,

	TRID_NEW = 100,
	TRID_EDIT,
	TRID_DELETE,
	TRID_UP,
	TRID_DOWN
};

//-------------------

//1bit (15x5)x15
static const unsigned char g_img_iconbar[]={
0x00,0x00,0xff,0x1f,0x00,0x00,0x00,0x00,0x00,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,
0x10,0x00,0x1c,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,0x38,0x00,0x1c,0x00,0xc0,0x01,
0xfd,0x17,0x00,0x00,0x7c,0x00,0x1c,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,0xfe,0x00,
0x1c,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,0xff,0x01,0x1c,0x00,0xfe,0x3f,0x01,0x90,
0xff,0x8f,0xff,0x03,0x1c,0x00,0xfe,0x3f,0xfd,0x97,0xff,0xcf,0xff,0xe7,0xff,0x03,
0xfe,0x3f,0x01,0x90,0xff,0x0f,0x38,0xc0,0xff,0x01,0xc0,0x01,0x01,0x90,0xff,0x0f,
0x38,0x80,0xff,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,0x38,0x00,0x7f,0x00,0xc0,0x01,
0xfd,0x17,0x00,0x00,0x38,0x00,0x3e,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,0x38,0x00,
0x1c,0x00,0xc0,0x01,0x01,0x10,0x00,0x00,0x38,0x00,0x08,0x00,0x00,0x00,0xff,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00 };

//-------------------


//*********************************
// 編集ダイアログ
//*********************************


/* イベント */

static int _edit_event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id == MLK_WID_OK)
	{
		//OK 時、値を確認

		_dialog_edit *p = (_dialog_edit *)wg;

		if(mLineEditIsEmpty(p->le_name)
			|| mLineEditIsEmpty(p->le_text))
			return 1;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}

/* ダイアログ作成 */

static _dialog_edit *_create_edit_dialog(mWindow *parent,mStr *strname,mStr *strtext)
{
	_dialog_edit *p;

	p = (_dialog_edit *)mDialogNew(parent, sizeof(_dialog_edit), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _edit_event_handle;
	p->ct.sep = 6;

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_EDIT));

	//表示名

	mLabelCreate(MLK_WIDGET(p), 0, 0, 0, MLK_TR(TRID_NAME));

	p->le_name = mLineEditCreate(MLK_WIDGET(p), 0, MLF_EXPAND_W, 0, 0);

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(p->le_name), 20, 0);

	mLineEditSetText(p->le_name, strname->buf);

	//テキスト

	mLabelCreate(MLK_WIDGET(p), 0, 0, 0, MLK_TR(TRID_TEXT));

	p->le_text = mLineEditCreate(MLK_WIDGET(p), 0, MLF_EXPAND_W, 0, 0);

	mLineEditSetText(p->le_text, strtext->buf);

	//OK/Cancel

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,9,0,0));

	return p;
}

/* 編集ダイアログ実行 */

static mlkbool _editdialog_run(mWindow *parent,mStr *strname,mStr *strtext)
{
	_dialog_edit *p;
	mlkbool ret;

	p = _create_edit_dialog(parent, strname, strtext);
	if(!p) return FALSE;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	ret = mDialogRun(MLK_DIALOG(p), FALSE);
	if(ret)
	{
		mLineEditGetTextStr(p->le_name, strname);
		mLineEditGetTextStr(p->le_text, strtext);
	}

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}



//*********************************
// リストのダイアログ
//*********************************


/* 新規追加 */

static void _cmd_new(_dialog *p)
{
	mStr strname = MSTR_INIT,strtext = MSTR_INIT;
	mColumnItem *pi;

	if(mListViewGetItemNum(p->list) > 100)
		return;

	if(_editdialog_run(MLK_WINDOW(p), &strname, &strtext))
	{
		pi = mListViewAddItem_text_copy_param(p->list,
			strname.buf, (intptr_t)mStrdup(strtext.buf));

		mListViewSetFocusItem(p->list, pi);
	}

	mStrFree(&strname);
	mStrFree(&strtext);
}

/* 編集 */

static void _cmd_edit(_dialog *p)
{
	mStr strname = MSTR_INIT,strtext = MSTR_INIT;
	mColumnItem *pi;

	pi = mListViewGetFocusItem(p->list);
	if(!pi) return;

	mStrSetText(&strname, pi->text);
	mStrSetText(&strtext, (char *)pi->param);

	if(_editdialog_run(MLK_WINDOW(p), &strname, &strtext))
	{
		mListViewSetItemText_copy(p->list, pi, strname.buf);

		mFree((void *)pi->param);
		pi->param = (intptr_t)mStrdup(strtext.buf);
	}

	mStrFree(&strname);
	mStrFree(&strtext);
}

/* 削除 */

static void _cmd_delete(_dialog *p)
{
	mColumnItem *pi;

	pi = mListViewGetFocusItem(p->list);
	if(!pi) return;

	mFree((void *)pi->param);

	mListViewDeleteItem_focus(p->list);
}

/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	_dialog *p = (_dialog *)wg;

	switch(ev->type)
	{
		case MEVENT_NOTIFY:
			if(ev->notify.id == WID_LIST
				&& ev->notify.notify_type == MLISTVIEW_N_ITEM_L_DBLCLK)
			{
				//ダブルクリックで編集
				_cmd_edit(p);
			}
			break;
			
		case MEVENT_COMMAND:
			switch(ev->cmd.id)
			{
				//新規
				case TRID_NEW:
					_cmd_new(p);
					break;
				//編集
				case TRID_EDIT:
					_cmd_edit(p);
					break;
				//削除
				case TRID_DELETE:
					_cmd_delete(p);
					break;
				//上へ/下へ
				case TRID_UP:
				case TRID_DOWN:
					mListViewMoveItem_updown(p->list, NULL, (ev->cmd.id == TRID_DOWN));
					break;
			}
			return 1;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}

/* リストセット */

static void _set_list(_dialog *p)
{
	mListView *list = p->list;
	SampleItem *pi;

	MLK_LIST_FOR(APPDATA->list_sample, pi, SampleItem)
	{
		mListViewAddItem_text_copy_param(list, pi->name, (intptr_t)mStrdup(pi->text));
	}
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;
	mIconBar *ib;
	int i;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	MLK_TRGROUP(TRGROUP_ID_DLG_SAMPLE);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	//リスト

	p->list = mListViewCreate(MLK_WIDGET(p), WID_LIST, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_AUTO_WIDTH, MSCROLLVIEW_S_HORZVERT_FRAME);

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(p->list), 18, 12);

	//mIconBar

	ib = mIconBarCreate(MLK_WIDGET(p), 0, 0, MLK_MAKE32_4(0,3,0,0),
		MICONBAR_S_TOOLTIP | MICONBAR_S_DESTROY_IMAGELIST);

	mIconBarSetTooltipTrGroup(ib, TRGROUP_ID_DLG_SAMPLE);
	mIconBarSetImageList(ib, mImageListCreate_1bit_textcol(g_img_iconbar, 75, 15, 15));

	for(i = 0; i < 5; i++)
		mIconBarAdd(ib, TRID_NEW + i, i, TRID_NEW + i, 0);

	//OK/Cancel

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,5,0,0));

	_set_list(p);

	return p;
}

/* リストのアイテムで確保されたバッファを解放 (キャンセル時) */

static void _free_listitem(mListView *list)
{
	mColumnItem *pi;

	pi = mListViewGetTopItem(list);

	for(; pi; pi = (mColumnItem *)pi->i.next)
		mFree((void *)pi->param);
}

/* データを適用 */

static void _apply_data(mListView *list)
{
	mList *dstlist = &APPDATA->list_sample;
	mColumnItem *pi;
	SampleItem *si;

	mListDeleteAll(dstlist);

	//

	pi = mListViewGetTopItem(list);

	for(; pi; pi = (mColumnItem *)pi->i.next)
	{
		si = (SampleItem *)mListAppendNew(dstlist, sizeof(SampleItem));
		if(si)
		{
			si->name = mStrdup(pi->text);
			si->text = (char *)pi->param;
		}
	}
}

/** ダイアログ実行 */

void SampleListDlg_run(mWindow *parent)
{
	_dialog *p;

	p = _create_dialog(parent);
	if(!p) return;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	//OK 時は、アイテムの param のポインタをそのまま利用する。
	//キャンセル時は、解放。

	if(mDialogRun(MLK_DIALOG(p), FALSE))
		_apply_data(p->list);
	else
		_free_listitem(p->list);

	mWidgetDestroy(MLK_WIDGET(p));
}
