/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * メニューデータ
 **********************************/

static const uint16_t g_mainmenu_data[] = {
//ファイル
TRID_MENU_TOP_FILE,
MMENUBAR_ARRAY16_SUB_START,
	TRID_MENU_FILE_OPENDIR, MMENUBAR_ARRAY16_SEP,
	TRID_MENU_FILE_SYSTEMDIR, TRID_MENU_FILE_RECENT, MMENUBAR_ARRAY16_SEP,
	TRID_MENU_FILE_EXIT,
MMENUBAR_ARRAY16_SUB_END,

//表示
TRID_MENU_TOP_VIEW,
MMENUBAR_ARRAY16_SUB_START,
	TRID_MENU_VIEW_PREV_HORZ | MMENUBAR_ARRAY16_CHECK,
	TRID_MENU_VIEW_PREV_VERT | MMENUBAR_ARRAY16_CHECK, MMENUBAR_ARRAY16_SEP,
	TRID_MENU_VIEW_ASCENT | MMENUBAR_ARRAY16_CHECK,
	TRID_MENU_VIEW_BASELINE | MMENUBAR_ARRAY16_CHECK,
	TRID_MENU_VIEW_VORG | MMENUBAR_ARRAY16_CHECK,
MMENUBAR_ARRAY16_SUB_END,

//ヘルプ
TRID_MENU_TOP_HELP,
MMENUBAR_ARRAY16_SUB_START,
	TRID_MENU_HELP_TIPS, TRID_MENU_HELP_VERSION,
MMENUBAR_ARRAY16_SUB_END,

MMENUBAR_ARRAY16_END
};
