/*$
azfontviewer
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow - sub
 *****************************************/

#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_listview.h"
#include "mlk_combobox.h"
#include "mlk_lineedit.h"
#include "mlk_tab.h"
#include "mlk_columnitem.h"
#include "mlk_sysdlg.h"
#include "mlk_event.h"
#include "mlk_menu.h"
#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_dir.h"
#include "mlk_file.h"
#include "mlk_string.h"
#include "mlk_font.h"
#include "mlk_fontinfo.h"

#include "def_mainwin.h"
#include "def_appdata.h"
#include "font.h"
#include "trid.h"


//----------------

enum
{
	FILEICON_PARENT,
	FILEICON_DIR,
	FILEICON_FILE
};

void SampleListDlg_run(mWindow *parent);

//----------------


//=========================
// ファイルリスト
//=========================


/* リストのソート関数 */

static int _sortfunc_list(mListItem *item1,mListItem *item2,void *param)
{
	mColumnItem *p1,*p2;

	p1 = (mColumnItem *)item1;
	p2 = (mColumnItem *)item2;

	if(p1->param < p2->param)
		return -1;
	else if(p1->param > p2->param)
		return 1;
	else
		return strcmp(p1->text, p2->text);
}

/** ファイルリストをセット */

void MainWindow_setFileList(MainWindow *p)
{
	mListView *list = p->list;
	mDir *dir;
	const char *filter,*name;
	int n,is_root;

	mListViewDeleteAllItem(list);

	//存在しないディレクトリなら、空

	if(!mIsExistDir(APPDATA->strPath.buf))
		return;

	//フィルタ文字列

	filter = (const char *)mComboBoxGetItemParam(p->cb_filter, -1);

	//トップディレクトリか

	is_root = mStrPathIsTop(&APPDATA->strPath);

	//--------

	dir = mDirOpen(APPDATA->strPath.buf);
	if(!dir) return;

	while(mDirNext(dir))
	{
		if(mDirIsSpecName(dir))
		{
			if(mDirIsSpecName_parent(dir) && !is_root)
				mListViewAddItem(list, "..", FILEICON_PARENT, 0, FILEICON_PARENT);
			continue;
		}

		name = mDirGetFilename(dir);
		n = (mDirIsDirectory(dir))? FILEICON_DIR: FILEICON_FILE;

		//フィルタによる除外

		if(n == FILEICON_FILE && filter)
		{
			if(mStringMatch_sum(name, filter, TRUE) == -1)
				continue;
		}

		mListViewAddItem(list, name, n, MCOLUMNITEM_F_COPYTEXT, n);
	}

	mDirClose(dir);

	//

	mListViewSortItem(list, _sortfunc_list, 0);
}

/** パスが変わった時 */

void MainWindow_onChangePath(MainWindow *p)
{
	MainWindow_setFileList(p);

	mLineEditSetText(p->le_dir, APPDATA->strPath.buf);
}

/** ファイルリスト:親へ移動 */

void MainWindow_filelist_moveParent(MainWindow *p)
{
	mStr str = MSTR_INIT;

	mStrPathGetBasename(&str, APPDATA->strPath.buf);

	mStrPathRemoveBasename(&APPDATA->strPath);
	
	MainWindow_onChangePath(p);

	//元のディレクトリを選択

	if(mStrIsnotEmpty(&str))
		mListViewSetFocusItem_text(p->list, str.buf);

	mStrFree(&str);
}

/** ファイルリスト:選択ディレクトリへ移動 */

void MainWindow_filelist_moveSelDir(MainWindow *p)
{
	mColumnItem *pi;

	pi = mListViewGetFocusItem(p->list);

	if(pi && pi->param == FILEICON_DIR)
	{
		mStrPathJoin(&APPDATA->strPath, pi->text);

		MainWindow_onChangePath(p);

		mListViewSetFocusItem_index(p->list, 0);
	}
}

/** ファイルから、ディレクトリとフォントをセット */

void MainWindow_setFromFile(MainWindow *p,const char *filename)
{
	mStr str = MSTR_INIT;

	//ディレクトリ変更
	
	mStrPathGetDir(&APPDATA->strPath, filename);

	MainWindow_onChangePath(p);

	//ファイル選択

	mStrPathGetBasename(&str, filename);

	mListViewSetFocusItem_text(p->list, str.buf);

	//フォントセット

	MainWindow_setFontFile(p, str.buf, 0);

	mStrFree(&str);
}


//=========================
// フォントセット
//=========================


/* 各インデックスの名前列挙関数 */

static void _func_collection_names(int index,const char *name,void *param)
{
	mStr str = MSTR_INIT;

	if(index >> 16)
		//バリアブルフォント
		mStrSetFormat(&str, "[v%d] %s", (index >> 16) - 1, name);
	else
		mStrSetFormat(&str, "[%d] %s", index, name);

	mComboBoxAddItem_copy(MLK_COMBOBOX(param), str.buf, index);

	mStrFree(&str);
}

/* インデックスリストセット */

static void _set_index_list(MainWindow *p)
{
	mComboBoxDeleteAllItem(p->cb_fontindex);

	mFontGetListNames(mGuiGetFontSystem(),
		APPDATA->fontinfo.str_file.buf,
		_func_collection_names, p->cb_fontindex);

	mComboBoxSetSelItem_atIndex(p->cb_fontindex, 0);
}

/* フォント情報セット */

static void _set_fontinfo(MainWindow *p)
{
	mListView *list = p->list_fontinfo;
	mColumnItem *pi;
	mFont *font = APPDATA->font;
	char m[16];
	mStr str = MSTR_INIT;

	pi = mListViewGetTopItem(list);

	//フォント名

	mListViewSetItemColumnText(list, pi, 1, FontGetName(font));
	pi = MLK_COLUMNITEM(pi->i.next);

	//スタイル名 

	mListViewSetItemColumnText(list, pi, 1, FontGetStyleName(font));
	pi = MLK_COLUMNITEM(pi->i.next);

	//バージョン

	FontGetVersionText(font, &str);
	mListViewSetItemColumnText(list, pi, 1, str.buf);
	pi = MLK_COLUMNITEM(pi->i.next);

	//EM

	mIntToStr(m, FontGetEM(font));

	mListViewSetItemColumnText(list, pi, 1, (font)? m: NULL);
	pi = MLK_COLUMNITEM(pi->i.next);

	//ビットマップ

	mListViewSetItemColumnText(list, pi, 1, FontGetHaveBitmap(font));
	pi = MLK_COLUMNITEM(pi->i.next);

	//縦書き

	FontGetVert(font, &str);
	mListViewSetItemColumnText(list, pi, 1, str.buf);
	pi = MLK_COLUMNITEM(pi->i.next);

	//CID-ROS

	FontGetCID_ROS(font, &str);
	mListViewSetItemColumnText(list, pi, 1, str.buf);

	mStrFree(&str);
}

/* ディレクトリ履歴を追加 */

static void _add_recent_dir(MainWindow *p,const char *filename)
{
	mStr str = MSTR_INIT;

	mStrPathGetDir(&str, filename);

	if(!mStrPathCompareEq(APPDATA->strRecentDir, str.buf))
	{
		mStrArrayAddRecent(APPDATA->strRecentDir, RECENT_NUM, str.buf);

		//メニュー

		mMenuDeleteAll(p->menu_recent);
		mMenuAppendStrArray(p->menu_recent, APPDATA->strRecentDir, MAINWIN_CMDID_RECENT_DIR, RECENT_NUM);
	}

	mStrFree(&str);
}

/** フォント選択変更
 *
 * fname: ファイル変更時は、ファイル名。NULL でファイルはそのまま。
 * index: 負の値で、そのまま */

void MainWindow_setFontFile(MainWindow *p,const char *fname,int index)
{
	AppData *app = APPDATA;
	mFontInfo *info;

	mFontFree(app->font);
	app->font = NULL;

	//フォント情報

	info = &app->fontinfo;

	if(fname)
	{
		mStrCopy(&info->str_file, &app->strPath);
		mStrPathJoin(&info->str_file, fname);
	}

	if(index >= 0)
		info->index = index;

	//フォント作成
	// :フォントでない場合など、失敗時は NULL が返る

	app->font = FontCreate(mGuiGetFontSystem(), info);

	//成功時、ディレクトリ履歴追加

	if(app->font)
		_add_recent_dir(p, info->str_file.buf);

	//------- ウィジェット

	if(fname)
	{
		_set_index_list(p);
		_set_fontinfo(p);
	}

	MainWindow_updatePreview(p);
}

/** プレビュー更新 */

void MainWindow_updatePreview(MainWindow *p)
{
	mWidgetRedraw(p->prev_horz);
	mWidgetRedraw(p->prev_vert);
}


//=========================
// イベントハンドラ
//=========================


/* mFontInfo 拡張情報の更新 */

static void _change_infoex(MainWindow *p)
{
	mFontSetInfoEx(APPDATA->font, &APPDATA->fontinfo);

	MainWindow_updatePreview(p);
}

/* フォントサイズ変更時 */

static void _change_fontsize(MainWindow *p)
{
	AppData *app = APPDATA;

	app->fontinfo.size = app->fontsize / 10.0;

	if(app->fontsize_unit)
		app->fontinfo.size = -app->fontinfo.size;

	//

	mFontSetSize(app->font, app->fontinfo.size);

	MainWindow_updatePreview(p);
}

/* パスの履歴ボタン: メニュー表示 */

static void _runmenu_recent(MainWindow *p,mWidget *wg)
{
	mBox box;

	mWidgetGetBox_rel(wg, &box);

	mMenuPopup(p->menu_recent, wg, 0, 0, &box,
		MPOPUP_F_RIGHT | MPOPUP_F_BOTTOM | MPOPUP_F_GRAVITY_LEFT | MPOPUP_F_MENU_SEND_COMMAND,
		MLK_WIDGET(p));
}

/* 現在のテキストをサンプルに追加 */

static void _add_sample_current(MainWindow *p)
{
	mStr str = MSTR_INIT;
	const char *name;
	SampleItem *pi;

	if(mLineEditIsEmpty(p->le_sample))
		return;

	//名前を指定

	name = MLK_TR(TRID_SAMPLEMENU_NAME);

	if(!mSysDlg_inputText(MLK_WINDOW(p), name, name,
		MSYSDLG_INPUTTEXT_F_NOT_EMPTY, &str))
		return;

	//追加

	pi = (SampleItem *)mListAppendNew(&APPDATA->list_sample, sizeof(SampleItem));
	if(pi)
	{
		pi->name = mStrdup(str.buf);

		mLineEditGetTextStr(p->le_sample, &str);

		pi->text = mStrdup(str.buf);
	}

	mStrFree(&str);
}

/* サンプルのメニュー表示 */

static void _runmenu_sample(MainWindow *p,mWidget *wg)
{
	mMenu *menu;
	mMenuItem *mi;
	SampleItem *si;
	mBox box;
	int id;

	menu = mMenuNew();

	MLK_TRGROUP(TRGROUP_ID_SAMPLE_MENU);

	//リスト

	MLK_LIST_FOR(APPDATA->list_sample, si, SampleItem)
	{
		mMenuAppendText_param(menu, -2, si->name, (intptr_t)si);
	}

	//

	if(APPDATA->list_sample.top)
		mMenuAppendSep(menu);

	mMenuAppendTrText(menu, TRID_SAMPLEMENU_ADD, 2);

	//

	mWidgetGetBox_rel(wg, &box);

	mi = mMenuPopup(menu, wg, 0, 0, &box,
		MPOPUP_F_RIGHT | MPOPUP_F_BOTTOM | MPOPUP_F_GRAVITY_LEFT, NULL);

	if(!mi)
		id = -1;
	else
	{
		id = mMenuItemGetID(mi);
		si = (SampleItem *)mMenuItemGetParam1(mi);
	}

	mMenuDestroy(menu);

	//-------

	if(id == -1)
		return;
	else if(id == -2)
	{
		//サンプルセット

		mStrSetText(&APPDATA->strSample, si->text);

		mLineEditSetText(p->le_sample, si->text);
		
		MainWindow_updatePreview(p);
	}
	else
	{
		switch(id)
		{
			//追加
			case TRID_SAMPLEMENU_ADD:
				_add_sample_current(p);
				break;
			//編集
			case TRID_SAMPLEMENU_EDIT:
				SampleListDlg_run(MLK_WINDOW(p));
				break;
		}
	}
}

/* ファイルリストの通知 */

static void _notify_filelist(MainWindow *p,mEventNotify *ev)
{
	mColumnItem *item = (mColumnItem *)ev->param1;

	switch(ev->notify_type)
	{
		//フォーカス変更
		case MLISTVIEW_N_CHANGE_FOCUS:
			if(item->param == FILEICON_FILE)
				MainWindow_setFontFile(p, item->text, 0);
			break;
		//ダブルクリック
		case MLISTVIEW_N_ITEM_L_DBLCLK:
			if(item->param == FILEICON_PARENT)
			{
				//親へ

				MainWindow_filelist_moveParent(p);
			}
			else if(item->param == FILEICON_DIR)
			{
				//ディレクトリへ

				mStrPathJoin(&APPDATA->strPath, item->text);
				MainWindow_onChangePath(p);
			}
			break;
	}
}

/** 通知ハンドラ */

void MainWindow_notify_handle(MainWindow *p,mEventNotify *ev)
{
	int type = ev->notify_type;

	switch(ev->id)
	{
		//ファイルリスト
		case WID_LIST:
			_notify_filelist(p, ev);
			break;
		//フィルタ
		case WID_CB_FILTER:
			if(type == MCOMBOBOX_N_CHANGE_SEL)
				MainWindow_setFileList(p);
			break;
		//パス履歴ボタン
		case WID_BTT_RECENT:
			_runmenu_recent(p, ev->widget_from);
			break;
		//パスエディット (ENTER で移動)
		case WID_EDIT_DIR:
			if(type == MLINEEDIT_N_ENTER)
			{
				//"~" はホームディレクトリに
				mLineEditGetTextStr(p->le_dir, &APPDATA->strPath);
				mStrPathReplaceHome(&APPDATA->strPath);

				MainWindow_onChangePath(p);
			}
			break;
		//タブ (タブ内容切り替え)
		case WID_TAB_INFO:
			if(type == MTAB_N_CHANGE_SEL)
			{
				APPDATA->tabno = ev->param1;
				
				mWidgetShow(p->wg_tab_top[!APPDATA->tabno], 0);
				mWidgetShow(p->wg_tab_top[APPDATA->tabno], 1);

				mWidgetLayout(p->wg_tab_top[0]->parent);
			}
			break;

		//----- 情報

		//サンプル文字列 (mLineEdit)
		case WID_EDIT_SAMPLE:
			if(type == MLINEEDIT_N_CHANGE)
			{
				mLineEditGetTextStr(p->le_sample, &APPDATA->strSample);
				MainWindow_updatePreview(p);
			}
			break;
		//サンプルメニュー
		case WID_BTT_SAMPLE_MENU:
			_runmenu_sample(p, ev->widget_from);
			break;
		//フォントインデックス
		case WID_CB_FONTINDEX:
			if(type == MCOMBOBOX_N_CHANGE_SEL)
				MainWindow_setFontFile(p, NULL, ev->param2);
			break;

		//----- プレビュー設定

		//サイズ
		case WID_EDIT_FONTSIZE:
			if(type == MLINEEDIT_N_CHANGE)
			{
				APPDATA->fontsize = mLineEditGetNum(p->le_fontsize);
				_change_fontsize(p);
			}
			break;
		//サイズ単位
		case WID_CB_FONTSIZE:
			if(type == MCOMBOBOX_N_CHANGE_SEL)
			{
				APPDATA->fontsize_unit = ev->param2;
				_change_fontsize(p);
			}
			break;
		//ヒンティング
		case WID_CB_HINTING:
			if(type == MCOMBOBOX_N_CHANGE_SEL)
			{
				APPDATA->fontinfo.ex.hinting = APPDATA->hinting = ev->param2;
				_change_infoex(p);
			}
			break;
		//描画
		case WID_CB_RENDERING:
			if(type == MCOMBOBOX_N_CHANGE_SEL)
			{
				APPDATA->fontinfo.ex.rendering = APPDATA->rendering = ev->param2;
				_change_infoex(p);
			}
			break;
		//LCDフィルタ
		case WID_CB_LCDFILTER:
			if(type == MCOMBOBOX_N_CHANGE_SEL)
			{
				APPDATA->fontinfo.ex.lcd_filter = APPDATA->lcdfilter = ev->param2;
				_change_infoex(p);
			}
			break;
		//埋め込みビットマップ有効
		case WID_CK_ENABLE_BITMAP:
			APPDATA->fprevopt ^= PREVOPT_F_ENABLE_BITMAP;
			APPDATA->fontinfo.ex.flags ^= MFONTINFO_EX_FLAGS_EMBEDDED_BITMAP;

			_change_infoex(p);
			break;
		//オートヒント無効
		case WID_CK_DISABLE_AUTOHINT:
			APPDATA->fprevopt ^= PREVOPT_F_DISABLE_AUTOHINT;
			APPDATA->fontinfo.ex.flags ^= MFONTINFO_EX_FLAGS_AUTO_HINT;

			_change_infoex(p);
			break;
	}
}

